CREATE TABLE query (
  id_query int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name varchar(64) NOT NULL,
  email varchar(64) NOT NULL,
  birthyear int(4) NOT NULL,
  sex char(1) NOT NULL,
  limb char(1) NOT NULL,
  biography varchar(256)
);

CREATE TABLE superpowers (
  id_superpowers int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  list varchar(64) NOT NULL
);

CREATE TABLE superheroes (
  id_hero int(10) NOT NULL,
  id_superpowers int(10) NOT NULL
);