<?php
  header('Content-Type: text/html; charset=UTF-8');
  setlocale(LC_ALL, 'Russian_Russia.65001');

  try{
    $check = true;
    if (empty($_POST['name'])) {
        print("Поле с именем не заполнено.<br/>");
        $check = false;
    } if (empty($_POST['email'])) {
        print("Поле с почтой не заполнено.<br/>");
        $check = false;
    } if (empty($_POST['birthyear'])) {
        print("Поле с датой рождения не заполнено.<br/>");
        $check = false;
    } if (empty($_POST['sex'])) {
        print("Вы должны указать ваш пол.<br/>");
        $check = false;
    } if (empty($_POST['limb'])) {
        print("Вы должны указать количество ваших конечностей.<br/>");
        $check = false;
    } if (empty($_POST['superpowers'])) {
        print("Вы должны указать ваши сверхспособности.<br/>");
        $check = false;
    } if (empty($_POST['biography'])) {
        print("Поле с биографией не заполнено.<br/>");
        $check = false;
    } if (empty($_POST['confirm'])) {
        print("Перед отправкой формы вы должны ознакомиться с контрактом.<br/>");
        $check = false;
    }

    if ($check==false) exit;
    
    $db_host = 'localhost';
    $db_user = 'u20967';
    $db_password = '7306510';

    $bd = new PDO("mysql:host=$db_host; dbname=$db_user", $db_user, $db_password, array(PDO::ATTR_PERSISTENT => true));

    $query = $bd->prepare("INSERT INTO query SET name = ?, email = ?, birthyear = ?, sex = ?, limb = ?, biography = ?");
    $query -> execute([$_POST['name'], $_POST['email'], $_POST['birthyear'], $_POST['sex'], $_POST['limb'], $_POST['biography']]);
    $id_query = $bd->lastInsertId();

    $list = implode(', ',$_POST['superpowers']);
    $superpowers = $bd->prepare("INSERT INTO superpowers SET list = ?");
    $superpowers -> execute([$list]);
    $id_superpowers = $bd->lastInsertId();

    $superheroes = $bd->prepare("INSERT INTO superheroes SET id_hero = ?, id_superpowers = ?");
    $superheroes -> execute([$id_query, $id_superpowers]);

    echo "Запрос был успешно отправлен!";
  } catch(PDOException $e){
    echo "Произошла ошибка при отправке запроса!";
    print('Error : ' . $e->getMessage());
  }
?>